FROM alpine:edge

RUN apk update && \
    apk add make

WORKDIR /usr/src/git-syn
COPY . /usr/src/git-syn

RUN make install

ENTRYPOINT ["/usr/local/bin/git-syn"]
